<?php

namespace z0s\Cron\Api;

interface CronInterface
{
    public function handle(): void;
}
