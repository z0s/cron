<?php

namespace z0s\Commands;

use Poliander\Cron\CronExpression;
use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use z0s\Console\Api\ConsoleCommand;

class Cron extends ConsoleCommand
{
    protected string $signature = 'cron';
    protected string $description = 'Run cron jobs';

    public function __construct(
        protected Container $container
    ) {
        parent::__construct();
    }

    final public function handle(): void
    {
        $finder = new ComposerFinder($this->autoloader);
        $finder->inNamespace('z0s\\Cronjobs');

        foreach ($finder as $className => $reflection) {
            /** @var \z0s\Cron\Api\Cronjob $instance */
            $instance = $this->container->get($className);
            $cronTime = $instance->getCronTime();

            $cronExpression = new CronExpression($cronTime);
            $shouldRun = $cronExpression->isMatching();

            if ($shouldRun === true) {
                try {
                    $this->out('Running cronjob: ' . $className);
                    $instance->handle();
                } catch (\Exception $e) {
                    $this->out("Error while running cron job {$className}: {$e->getMessage()}");
                }
            }
        }
    }
}
